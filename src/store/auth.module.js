import AuthService from '../services/auth.service';

const user = JSON.parse(sessionStorage.getItem('user'));
const initialState = user
    ? { status: { loggedIn: true }, user }
    : { status: { loggedIn: false }, user: null };

export const auth = {
    namespaced: true,
    state: initialState,
    actions: {
        login({ commit }, user) {
            return AuthService.login(user).then(
                user => {
                    commit('loginSuccess', user);
                    return Promise.resolve(user);
                },
                error => {
                    console.log('loginFailure');
                    commit('loginFailure');
                    return Promise.reject(error);
                }
            );
        },
        register({ commit }, user) {
            return AuthService.register(user).then(
                user => {
                    commit('loginSuccess', user);
                    return Promise.resolve(user);
                },
                error => {
                    console.log('loginFailure');
                    commit('loginFailure');
                    return Promise.reject(error);
                }
            );
        },
        update({ commit }, data) {
            return AuthService.update(data, data.id).then(
                user => {
                    commit('updateUser', user);
                    return Promise.resolve(user);
                },
                error => {
                    console.log(error);
                    commit('loginFailure');
                }
            );
        },
        logout({ commit }) {
            sessionStorage.removeItem('user');
            commit('logout');
        },
    },
    mutations: {
        loginSuccess(state, user) {
            state.status.loggedIn = true;
            state.user = user;
        },
        updateData(state) {
            sessionStorage.setItem('user', JSON.stringify(state.user));
        },
        updateUser(state, user) {
            state.user.user = user;
        },
        loginFailure(state) {
            state.status.loggedIn = false;
            state.user = null;
        },
        logout(state) {
            state.status.loggedIn = false;
            state.user = null;
        },
    }
};
