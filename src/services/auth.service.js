import axios from 'axios';
import { API_URL } from '../helpers/_api'


class AuthService {
    login(user) {
        return axios
            .post(API_URL + 'login', {
                email: user.email,
                password: user.password
            })
            .then(response => {
                const data = {
                    'token': response.data.token,
                    'user': response.data.user,
                }
                sessionStorage.setItem('user', JSON.stringify(data));

                return data;
            });
    }

    update(data, id) {
        return axios
            .put(API_URL + 'user/' + id, data)
            .then(response => {
                return response.data;
            });
    }

    register(data) {
        return axios
            .post(API_URL + 'register', data)
            .then(response => {
                const data = {
                    'token': response.data.token,
                    'user': response.data.user,
                }
                sessionStorage.setItem('user', JSON.stringify(data));

                return data;
            });
    }
}

export default new AuthService();
