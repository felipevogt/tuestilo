import axios from 'axios';
import { API_URL, API_HEADER_FORM } from '../helpers/_api'


class ServiciosService {
    store(data, id) {
        return axios
            .post(API_URL + 'establecimiento/' + id + '/servicios', data, API_HEADER_FORM)
            .then(response => {
                return response.data;
            });
    }

    update(data, idServicio) {
        return axios
            .post(API_URL + 'servicio/' + idServicio, data, API_HEADER_FORM)
            .then(response => {
                return response.data;
            });
    } 

    delete(idServicio) {
        return axios
            .delete(API_URL + 'servicio/' + idServicio)
            .then(response => {
                return response.data;
            });
    }
    getServicios(id) {
        return axios
            .get(API_URL + 'establecimiento/' + id + '/servicios')
            .then(response => {
                return response.data;
            });
    }
    getPersonal(id) {
        return axios
            .get(API_URL + 'establecimiento/' + id + '/personalSimple')
            .then(response => {
                return response.data;
            });
    }

    getHoras(data) {
        return axios
            .post(API_URL + 'agenda/obtenerHorasDisponibles', data)
            .then(response => {
                return response.data;
            });
    }

    reservarHora(data){
        return axios
        .post(API_URL + 'agenda/reservarHora', data)
        .then(response => {
            return response.data;
        });
    }

    getReservas(id) {
        return axios
            .get(API_URL + 'establecimiento/' + id + '/reservas')
            .then(response => {
                return response.data;
            });
    }

    getReservasConfirmadas(id) {
        return axios
            .get(API_URL + 'establecimiento/' + id + '/reservasConfirmadas')
            .then(response => {
                return response.data;
            });
    }

    getReservasCliente(id) {
        return axios
            .get(API_URL + 'cliente/' + id + '/reservas')
            .then(response => {
                return response.data;
            });
    }

    confirmarReserva(id, data){
        return axios
        .post(API_URL + 'reserva/' + id + '/confirmar', data)
        .then(response => {
            return response.data;
        });
    }
}

export default new ServiciosService();
